import "@babel/polyfill";
import {} from "dotenv/config";
import app from "./app";

// Initialize App
const port = process.env.PORT || 4000;
app.listen(port);
