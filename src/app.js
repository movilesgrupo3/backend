import express from "express";
import morgan from "morgan";
import cors from "cors";

// Import Setup
//import setup from "./setup";
// import prueba from "./pruebaSDK";

// prueba();

// Import DB
import db from "./database/database";

// Import routes
import cityRoutes from "./routes/city.routes";
import neighborhoodGroupRoutes from "./routes/neighborhoodGroup.routes";
import ownerRoutes from "./routes/owner.routes";
import walkerRoutes from "./routes/walker.routes";
import breedRoutes from "./routes/breed.routes";
import dogRoutes from "./routes/dog.routes";
import walkRoutes from "./routes/walk.routes";
import reportRoutes from "./routes/report.routes";

// Create Express App
const app = express();

// Middlewares
app.use(morgan("dev"));
app.use(express.json());
app.use(cors());

// Routes
app.use("/api/cities", cityRoutes);
app.use("/api/neighborhoodGroups", neighborhoodGroupRoutes);
app.use("/api/owners", ownerRoutes);
app.use("/api/walkers", walkerRoutes);
app.use("/api/breeds", breedRoutes);
app.use("/api/dogs", dogRoutes);
app.use("/api/walks", walkRoutes);
app.use("/api/reports", reportRoutes);

// Sync db
(async () => {
  try {
    //await db.sync({ force: true });
    await db.sync();

    //await setup();
    console.log("DB Connected");
  } catch (error) {
    console.log(error);
  }
})();

export default app;
