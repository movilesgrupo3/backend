import Sequelize from "sequelize";

const dbURL = process.env.DATABASE_URL;

const db = new Sequelize(dbURL, {
  dialect: "postgres",
  protocol: "postgres",
  dialectOptions: {
    ssl: {
      rejectUnauthorized: false,
    },
  },
});

export default db;
