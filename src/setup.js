import faker from "faker";
import path from "path";
import cloudinary from "cloudinary";

import { FeeTypeEnumConf } from "./common/enums";

import User from "./models/User";
import Manufacturer from "./models/Manufacturer";
import Client from "./models/Client";

import CatalogItem from "./models/CatalogItem";

import PrintSettingsCategory from "./models/PrintSettingsCategory";
import PrintSetting from "./models/PrintSetting";

import Printer from "./models/Printer";

import PrintSettingFee from "./models/PrintSettingFee";

import PrintSpecification from "./models/PrintSpecification";

const setup = async () => {
  // Users
  await setupUsers();

  // Catalog Items
  await setupCatalogItems();

  // Print Setting Categories
  await setupPrintSettingsCategories();

  // Print Settings
  await setupPrintSettings();

  // Printers
  await setupPrinters();

  // Print Settings Fees
  await setupPrintSettingFees();

  // Print Specifications
  await setupPrintSpecifications();
};

const manufacturers = [
  {
    name: "Departamento de Ingeniería Industrial",
    printers: [{ brand: "Makerbot", name: "Replicator+" }],
  },
  {
    name: "Departamento de Ingeniería Mecánica",
    printers: [
      { brand: "XYZ Printing", name: "da Vinci 1.0" },
      { brand: "Makerbot", name: "Replicator+" },
      { brand: "Formlabs", name: "Formlabs 1+" },
    ],
  },
  {
    name: "Departamento de Ingeniería Biomédica",
    printers: [
      { brand: "Ultimaker", name: "Ultimaker 2+" },
      { brand: "Ultimaker", name: "Ultimaker 3" },
      { brand: "Stratasys", name: "Object 30 Pro" },
    ],
  },
];

const catalog = [
  {
    id: "chasisInferior",
    name: "Chasis Inferior",
    description: "Pieza de soporte del chasis del aerodeslizador.",
    dimensions: [48, 6.7, 29],
  },
  {
    id: "chasisSuperior",
    name: "Chasis Superior",
    description: "Pieza superior del chasis del aerodeslizador.",
    dimensions: [50, 7.6, 30],
  },
  {
    id: "alabeDireccion",
    name: "Álabe Dirección",
    description: "Paleta del rotor de dirección del aerodeslizador.",
    dimensions: [11.3, 26.1, 0.6],
  },
  {
    id: "barraDireccion",
    name: "Barra Dirección",
    description: "Pieza de control de dirección del aerodeslizador.",
    dimensions: [13.5, 0.5, 0.2],
  },
  {
    id: "laminaFaldaSuperior",
    name: "Lámina Falda Superior",
    description: "Pieza superior de la falda del aerodeslizador.",
    dimensions: [50, 0.3, 30],
  },
  {
    id: "laminaFaldaInferior",
    name: "Lámina Falda Inferior",
    description: "Pieza de soporte de la falda del aerodeslizador.",
    dimensions: [38, 0.3, 26],
  },
  {
    id: "pestana",
    name: "Pestaña",
    description: "Pieza de soporte del rotor del aerodeslizador.",
    dimensions: [0.5, 15, 10],
  },
  {
    id: "soporteElectronica",
    name: "Soporte para Electrónica",
    description: "Pieza de soporte para dispositivos de control.",
    dimensions: [26.1, 5.3, 5],
  },
];

const categories = [
  {
    id: null,
    name: "Material de impresión",
    settings: [
      { name: "PLA", feeType: FeeTypeEnumConf.material },
      { name: "ABS", feeType: FeeTypeEnumConf.material },
      { name: "PETG", feeType: FeeTypeEnumConf.material },
    ],
  },
  {
    id: null,
    name: "Color del filamento",
    settings: [
      { name: "Blanco", feeType: FeeTypeEnumConf.material },
      { name: "Amarillo", feeType: FeeTypeEnumConf.material },
      { name: "Azul", feeType: FeeTypeEnumConf.material },
      { name: "Rojo", feeType: FeeTypeEnumConf.material },
      { name: "Negro", feeType: FeeTypeEnumConf.material },
    ],
  },
  {
    id: null,
    name: "Entramado de la impresión",
    settings: [
      { name: "Rectangular", feeType: FeeTypeEnumConf.material },
      { name: "Triangular", feeType: FeeTypeEnumConf.material },
      { name: "Zig-Zag", feeType: FeeTypeEnumConf.material },
    ],
  },
  {
    id: null,
    name: "Diámetro de la boquilla",
    settings: [
      { name: "0.1 mm", feeType: FeeTypeEnumConf.material },
      { name: "0.2 mm", feeType: FeeTypeEnumConf.material },
      { name: "0.3 mm", feeType: FeeTypeEnumConf.material },
    ],
  },
  {
    id: null,
    name: "Orientación de la impresión",
    settings: [
      { name: "Horizontal", feeType: FeeTypeEnumConf.material },
      { name: "Vertical", feeType: FeeTypeEnumConf.material },
    ],
  },
  {
    id: null,
    name: "Altura de la capa",
    settings: [
      { name: "0.1 mm", feeType: FeeTypeEnumConf.material },
      { name: "0.2 mm", feeType: FeeTypeEnumConf.material },
      { name: "0.3 mm", feeType: FeeTypeEnumConf.material },
    ],
  },
  {
    id: null,
    name: "Porcentaje de relleno",
    settings: [
      { name: "30%", feeType: FeeTypeEnumConf.material },
      { name: "50%", feeType: FeeTypeEnumConf.material },
      { name: "70%", feeType: FeeTypeEnumConf.material },
      { name: "90%", feeType: FeeTypeEnumConf.material },
      { name: "100%", feeType: FeeTypeEnumConf.material },
    ],
  },
  {
    id: null,
    name: "Diámetro del filamento",
    settings: [
      { name: "0.1 mm", feeType: FeeTypeEnumConf.material },
      { name: "0.2 mm", feeType: FeeTypeEnumConf.material },
      { name: "0.3 mm", feeType: FeeTypeEnumConf.material },
    ],
  },
];

const setupUsers = async () => {
  try {
    // Manufacturers
    await Promise.all(
      manufacturers.map(async (manufacturer) => {
        const newUser = await User.create({
          name: manufacturer.name,
          email: faker.internet.email(),
        });

        const newManufacturer = await Manufacturer.create({
          userId: newUser.id,
          rating: faker.random.number({
            min: 1,
            max: 5,
            precision: 0.1,
          }),
        });
      })
    );

    // Clients
    for (let i = 0; i < 3; i++) {
      const newUser = await User.create({
        name: faker.name.firstName() + faker.name.lastName(),
        email: faker.internet.email(),
      });
      const newClient = await Client.create({ userId: newUser.id });
    }
  } catch (error) {
    console.log(error);
  }
};

const setupCatalogItems = async () => {
  try {
    await Promise.all(
      catalog.map(async (item) => {
        const uploadImageResult = await cloudinary.v2.uploader.upload(
          path.join(__dirname, `/public/images/${item.id}.png`),
          {
            folder: "images",
          }
        );

        const uploadFileResult = await cloudinary.v2.uploader.upload(
          path.join(__dirname, `/public/models/${item.id}.stl`),
          {
            folder: "models",
            resource_type: "auto",
            flags: `attachment:${item.id}`,
            fetch_format: "auto",
          }
        );

        const newCatalogItem = await CatalogItem.create({
          name: item.name,
          description: item.description,
          dimensions: item.dimensions,
          imageURL: uploadImageResult.secure_url,
          fileURL: uploadFileResult.secure_url,
          imagePublicId: uploadImageResult.public_id,
          filePublicId: uploadFileResult.public_id,
        });
      })
    );
  } catch (error) {
    console.log(error);
  }
};

// Categories
const setupPrintSettingsCategories = async () => {
  try {
    await Promise.all(
      categories.map(async (category, index) => {
        const newPrintSettingCategory = await PrintSettingsCategory.create({
          name: category.name,
        });
        categories[index].id = newPrintSettingCategory.dataValues.id;
      })
    );
  } catch (error) {
    console.log(error);
  }
};

const setupPrintSettings = async () => {
  try {
    await Promise.all(
      categories.map(async (category) => {
        category.settings.map(async (setting) => {
          const newPrintSetting = await PrintSetting.create({
            name: setting.name,
            printSettingsCategoryId: category.id,
          });
        });
      })
    );
  } catch (error) {
    console.log(error);
  }
};

const setupPrinters = async () => {
  try {
    //const manufacturers = await Manufacturer.findAll();

    await Promise.all(
      manufacturers.map(async (manufacturer) => {
        const dbManufacturer = await Manufacturer.findOne({
          include: [
            {
              model: User,
              as: "user",
              where: {
                name: manufacturer.name,
              },
            },
          ],
        });
        await Promise.all(
          manufacturer.printers.map(async (printer) => {
            const newPrinter = await Printer.create({
              brand: printer.brand,
              name: printer.name,
              maxDimensions: [
                faker.random.number({
                  min: 1,
                  max: 50,
                  precision: 0.1,
                }),
                faker.random.number({
                  min: 1,
                  max: 50,
                  precision: 0.1,
                }),
                faker.random.number({
                  min: 1,
                  max: 50,
                  precision: 0.1,
                }),
              ],
              manufacturerId: dbManufacturer.id,
            });
          })
        );
      })
    );
  } catch (error) {
    console.log(error);
  }
};

const setupPrintSettingFees = async () => {
  try {
    const printers = await Printer.findAll();
    const printSettings = await PrintSetting.findAll();

    await Promise.all(
      printers.map((printer) => {
        printSettings.map(async (printSetting) => {
          const newPrintSettingFee = await PrintSettingFee.create({
            fee: faker.random.number({
              min: 0.1,
              max: 2,
              precision: 0.1,
            }),
            printerId: printer.id,
            printSettingId: printSetting.id,
            feeType: FeeTypeEnumConf.material,
          });
        });
      })
    );
  } catch (error) {
    console.log(error);
  }
};

const setupPrintSpecifications = async () => {
  try {
    const printers = await Printer.findAll();
    const catalogItems = await CatalogItem.findAll();

    await Promise.all(
      printers.map((printer) => {
        catalogItems.map(async (catalogItem) => {
          const newPrintSpecification = await PrintSpecification.create({
            manufacturingTime: faker.random.number({
              min: 5,
              max: 500,
              precision: 0.1,
            }),
            filamentWeight: faker.random.number({
              min: 1,
              max: 500,
              precision: 0.1,
            }),
            filamentLength: faker.random.number({
              min: 0.1,
              max: 50,
              precision: 0.1,
            }),
            catalogItemId: catalogItem.id,
            printerId: printer.id,
          });
        });
      })
    );
  } catch (error) {
    console.log(error);
  }
};

export default setup;
