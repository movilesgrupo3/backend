import Sequelize from "sequelize";
import db from "../database/database";

import Owner from "./Owner";
import Walk from "./Walk";

const NeighborhoodGroup = db.define("NeighborhoodGroup", {
  id: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV4,
    primaryKey: true,
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true,
  },
});

// 1 to many association with Owner
NeighborhoodGroup.hasMany(Owner, {
  onDelete: "CASCADE",
  foreignKey: "neighborhoodGroupId",
});
Owner.belongsTo(NeighborhoodGroup, {
  as: "neighborhoodGroup",
  foreignKey: "neighborhoodGroupId",
});

// 1 to many association with Walk
NeighborhoodGroup.hasMany(Walk, {
  onDelete: "CASCADE",
  foreignKey: "neighborhoodGroupId",
});
Walk.belongsTo(NeighborhoodGroup, {
  as: "neighborhoodGroup",
  foreignKey: "neighborhoodGroupId",
});

export default NeighborhoodGroup;
