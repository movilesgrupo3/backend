import * as admin from "firebase-admin";
import serviceAccount from "./common/canem-app-firebase-adminsdk.json";

const prueba = async () => {
  let firebaseDefaultApp = admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
  });

  let defaultAuth = firebaseDefaultApp.auth();

  try {
    const idToken =
      "1eyJhbGciOiJSUzI1NiIsImtpZCI6ImY4NDY2MjEyMTQxMjQ4NzUxOWJiZjhlYWQ4ZGZiYjM3ODYwMjk5ZDciLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vY2FuZW0tYXBwIiwiYXVkIjoiY2FuZW0tYXBwIiwiYXV0aF90aW1lIjoxNjE3MTQzMDcwLCJ1c2VyX2lkIjoiZlZUWmdZWlZNN2YzcDI5SVh3Zzl2RjRFNm1KMyIsInN1YiI6ImZWVFpnWVpWTTdmM3AyOUlYd2c5dkY0RTZtSjMiLCJpYXQiOjE2MTcxNDMwNzAsImV4cCI6MTYxNzE0NjY3MCwiZW1haWwiOiJkYW5pZWxudWV2b0BnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsiZW1haWwiOlsiZGFuaWVsbnVldm9AZ21haWwuY29tIl19LCJzaWduX2luX3Byb3ZpZGVyIjoicGFzc3dvcmQifX0.jmKO_XB8XNukdruz2ZKedHIZBTs_HV6OPAoBMras3p45Xu8pD4FvQs6TiZ6fanvepbpOgJKjpLPke6p1cfnNhq_2vwejdJtbkcjOjkpPmyjIpgyL7bjtUSb2HwEKaV-qNjt8LojetuGbpTkhkVlfn0UVias9T8523c_4YKOaFnzNjIIaoNhdfscBToZdGEa_N3lrcQ_DBy_-8pf0uBut78aSwcpJjKgtC3QkIqKT7lr5i4C-qOmapnQPxKUGqJCwYxQCKqiE3nKHJpSknXoEFplcbD3UtQ6PFqanar49XXv_Glil9Drx38nLfmZejRn5n1Ojie6a1cbUccBrIZV41Q";
    const decodedToken = await defaultAuth.verifyIdToken(idToken);
    const uid = decodedToken.uid;
    console.log(decodedToken);
    console.log(uid);
  } catch (error) {
    console.log(error);
  }
};

export default prueba;
