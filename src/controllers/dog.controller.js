import Dog from "../models/Dog";

export const createDog = async (req, res, next) => {
  try {
    // Params
    const { name, age, imageURL, ownerId, breedId } = req.body;

    // Create Dog
    const newDog = await Dog.create({
      name,
      age,
      imageURL,
      ownerId,
      breedId,
    });

    res.json(newDog);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const getAllDogs = async (req, res, next) => {
  try {
    // Get all elements in database
    const dogs = await Dog.findAll();

    res.json(dogs);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const getDogById = async (req, res, next) => {
  try {
    // Params
    const { dogId } = req.params;

    // Find Dog in database
    const dog = await Dog.findOne({
      where: { id: dogId },
    });

    res.json(dog);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const updateDogById = async (req, res, next) => {
  try {
    // Params
    const { dogId } = req.params;
    const { name, age, imageURL, ownerId, breedId } = req.body;

    // Find Dog in database
    const dog = await Dog.findOne({
      where: { id: dogId },
    });

    // Update Dog Setting
    await dog.update({ name, age, imageURL, ownerId, breedId });

    res.json(dog);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const deleteDogById = async (req, res, next) => {
  try {
    // Params
    const { dogId } = req.params;

    // Delete Dog in database
    await Dog.destroy({
      where: { id: dogId },
    });

    res.json();
  } catch (error) {
    console.log(error);
    next(error);
  }
};
