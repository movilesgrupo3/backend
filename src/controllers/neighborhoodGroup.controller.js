import NeighborhoodGroup from "../models/NeighborhoodGroup";

export const createNeighborhoodGroup = async (req, res, next) => {
  try {
    // Params
    const { name, cityId } = req.body;

    // Create NeighborhoodGroup
    const newNeighborhoodGroup = await NeighborhoodGroup.create({
      name,
      cityId,
    });

    res.json(newNeighborhoodGroup);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const getAllNeighborhoodGroups = async (req, res, next) => {
  try {
    // Get all elements in database
    const neighborhoodGroups = await NeighborhoodGroup.findAll();

    res.json(neighborhoodGroups);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const getNeighborhoodGroupById = async (req, res, next) => {
  try {
    // Params
    const { neighborhoodGroupId } = req.params;

    // Find NeighborhoodGroup in database
    const neighborhoodGroup = await NeighborhoodGroup.findOne({
      where: { id: neighborhoodGroupId },
    });

    res.json(neighborhoodGroup);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const updateNeighborhoodGroupById = async (req, res, next) => {
  try {
    // Params
    const { neighborhoodGroupId } = req.params;
    const { name, cityId } = req.body;

    // Find NeighborhoodGroup in database
    const neighborhoodGroup = await NeighborhoodGroup.findOne({
      where: { id: neighborhoodGroupId },
    });

    // Update NeighborhoodGroup Setting
    await neighborhoodGroup.update({ name, cityId });

    res.json(neighborhoodGroup);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const deleteNeighborhoodGroupById = async (req, res, next) => {
  try {
    // Params
    const { neighborhoodGroupId } = req.params;

    // Delete NeighborhoodGroup in database
    await NeighborhoodGroup.destroy({
      where: { id: neighborhoodGroupId },
    });

    res.json();
  } catch (error) {
    console.log(error);
    next(error);
  }
};
