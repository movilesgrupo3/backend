import Breed from "../models/Breed";

export const createBreed = async (req, res, next) => {
  try {
    // Params
    const { name } = req.body;

    // Create Breed
    const newBreed = await Breed.create({
      name,
    });

    res.json(newBreed);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const getAllBreeds = async (req, res, next) => {
  try {
    // Get all elements in database
    const breeds = await Breed.findAll();

    res.json(breeds);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const getBreedById = async (req, res, next) => {
  try {
    // Params
    const { breedId } = req.params;

    // Find Breed in database
    const breed = await Breed.findOne({
      where: { id: breedId },
    });

    res.json(breed);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const updateBreedById = async (req, res, next) => {
  try {
    // Params
    const { breedId } = req.params;
    const { name } = req.body;

    // Find Breed in database
    const breed = await Breed.findOne({
      where: { id: breedId },
    });

    // Update Breed Setting
    await breed.update({ name });

    res.json(breed);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const deleteBreedById = async (req, res, next) => {
  try {
    // Params
    const { breedId } = req.params;

    // Delete Breed in database
    await Breed.destroy({
      where: { id: breedId },
    });

    res.json();
  } catch (error) {
    console.log(error);
    next(error);
  }
};
