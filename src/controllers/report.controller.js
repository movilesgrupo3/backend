import Report from "../models/Report";

export const createReport = async (req, res, next) => {
  try {
    // Params
    const { comment, rating, walkerId, ownerId, walkId } = req.body;

    const date = new Date();

    // Create Report
    const newReport = await Report.create({
      date,
      comment,
      rating,
      walkerId,
      ownerId,
      walkId,
    });

    res.json(newReport);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const getAllReports = async (req, res, next) => {
  try {
    // Get all elements in database
    const reports = await Report.findAll();

    res.json(reports);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const getReportById = async (req, res, next) => {
  try {
    // Params
    const { reportId } = req.params;

    // Find Report in database
    const report = await Report.findOne({
      where: { id: reportId },
    });

    res.json(report);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const updateReportById = async (req, res, next) => {
  try {
    // Params
    const { reportId } = req.params;
    const { comment, rating, walkerId, ownerId, walkId } = req.body;

    // Find Report in database
    const report = await Report.findOne({
      where: { id: reportId },
    });

    // Update Breed Setting
    await report.update({ comment, rating, walkerId, ownerId, walkId });

    res.json(report);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const deleteReportById = async (req, res, next) => {
  try {
    // Params
    const { reportId } = req.params;

    // Delete Report in database
    await Report.destroy({
      where: { id: reportId },
    });

    res.json();
  } catch (error) {
    console.log(error);
    next(error);
  }
};
