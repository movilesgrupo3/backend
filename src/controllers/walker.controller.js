import Walker from "../models/Walker";

export const createWalker = async (req, res, next) => {
  try {
    // Params
    const { name, email, uid, identification, rating, imageURL, status } =
      req.body;

    // Create Walker
    const newWalker = await Walker.create({
      name,
      email,
      uid,
      identification,
      rating,
      imageURL,
      status,
    });

    res.json(newWalker);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const getAllWalkers = async (req, res, next) => {
  try {
    // Get all elements in database
    const walkers = await Walker.findAll();

    res.json(walkers);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const getAllWalkersNear = async (req, res, next) => {
  try {
    // Params
    const { ownerPosition } = req.params;
    console.log(ownerPosition);

    // Find Owner in database
    const walkers = await Walker.findAll({
      where: { status: true },
    });

    let actualwalkers = [];
    let distances = [];
    walkers.map((walker) => {
      let latlon = walker.dataValues.identification;
      if (latlon != null) {
        let arraylatlon = latlon.split(";");
        let arraylatlonOwner = ownerPosition.split(";");
        let diflat = parseInt(arraylatlon[0]) - parseInt(arraylatlonOwner[0]);
        let diflon = parseInt(arraylatlon[1]) - parseInt(arraylatlonOwner[1]);
        let powdiflat = Math.pow(diflat, 2);
        let powdiflon = Math.pow(diflon, 2);
        let sumlatlon = powdiflat + powdiflon;
        let distanceFromOwner = Math.pow(sumlatlon, 0.5);
        let obj = {
          id: walker.id,
          name: walker.name,
          email: walker.email,
          uid: walker.uid,
          identification: walker.identification,
          rating: walker.rating,
          imageURL: walker.imageURL,
          status: walker.status,
          createdAt: walker.createdAt,
          updatedAt: walker.updatedAt,
          distance: distanceFromOwner,
        };
        let dist = distanceFromOwner;
        actualwalkers.push(obj);
        distances.push(dist);
      }
    });
    let respwalkers = [];
    distances.sort();
    for (let i = 0; i < 5; i++) {
      for (let j = 0; j < actualwalkers.length; j++) {
        if (actualwalkers[j].distance == distances[i]) {
          let obj = {
            id: actualwalkers[j].id,
            name: actualwalkers[j].name,
            email: actualwalkers[j].email,
            uid: actualwalkers[j].uid,
            identification: actualwalkers[j].identification,
            rating: actualwalkers[j].rating,
            imageURL: actualwalkers[j].imageURL,
            status: actualwalkers[j].status,
            createdAt: actualwalkers[j].createdAt,
            updatedAt: actualwalkers[j].updatedAt,
          };
          respwalkers.push(obj);
        }
      }
    }

    console.log(respwalkers);

    res.json(respwalkers);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const getWalkerById = async (req, res, next) => {
  try {
    // Params
    const { walkerId } = req.params;

    // Find Walker in database
    const walker = await Walker.findOne({
      where: { id: walkerId },
    });

    res.json(walker);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const getWalkerByUId = async (req, res, next) => {
  try {
    // Params
    const { walkerUId } = req.params;

    // Find Owner in database
    const walker = await Walker.findOne({
      where: { uid: walkerUId },
    });

    res.json(walker);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const updateWalkerById = async (req, res, next) => {
  try {
    // Params
    const { walkerId } = req.params;
    const { name, email, uid, identification, rating, imageURL, status } =
      req.body;

    // Find Walker in database
    const walker = await Walker.findOne({
      where: { id: walkerId },
    });

    // Update Walker Setting
    await walker.update({
      name,
      email,
      uid,
      identification,
      rating,
      imageURL,
      status,
    });

    res.json(walker);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const deleteWalkerById = async (req, res, next) => {
  try {
    // Params
    const { walkerId } = req.params;

    // Delete Walker in database
    await Walker.destroy({
      where: { id: walkerId },
    });

    res.json();
  } catch (error) {
    console.log(error);
    next(error);
  }
};
