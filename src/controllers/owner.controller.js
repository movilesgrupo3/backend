import Dog from "../models/Dog";
import Owner from "../models/Owner";

export const createOwner = async (req, res, next) => {
  try {
    // Params
    const { name, email, uid, imageURL, neighborhoodGroupId } = req.body;

    console.log(req.body);

    // Create Owner
    const newOwner = await Owner.create({
      name,
      email,
      uid,
      imageURL,
      neighborhoodGroupId,
    });

    res.json(newOwner);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const getAllOwners = async (req, res, next) => {
  try {
    // Get all elements in database
    const owners = await Owner.findAll();

    res.json(owners);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const getOwnerById = async (req, res, next) => {
  try {
    // Params
    const { ownerId } = req.params;

    // Find Owner in database
    const owner = await Owner.findOne({
      where: { id: ownerId },
    });

    res.json(owner);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const getOwnerFavoriteWalkers = async (req, res, next) => {
  try {
    // Params
    const { ownerId } = req.body;

    // Find Owner's favorite walkers in database
    const owner = await Owner.findOne({
      where: { id: ownerId },
    });

    const favorites = await owner.getFavoriteWalkers();

    res.json(favorites);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const getOwnerByUId = async (req, res, next) => {
  try {
    // Params
    const { ownerUId } = req.params;

    // Find Owner in database
    const owner = await Owner.findOne({
      where: { uid: ownerUId },
    });

    res.json(owner);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const updateOwnerById = async (req, res, next) => {
  try {
    // Params
    const { ownerId } = req.params;
    const { name, email, uid, imageURL, neighborhoodGroupId } = req.body;

    // Find Owner in database
    const owner = await Owner.findOne({
      where: { id: ownerId },
    });

    // Update Owner Setting
    await owner.update({ name, email, uid, imageURL, neighborhoodGroupId });

    res.json(owner);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const deleteOwnerById = async (req, res, next) => {
  try {
    // Params
    const { ownerId } = req.params;

    // Delete Owner in database
    await Owner.destroy({
      where: { id: ownerId },
    });

    res.json();
  } catch (error) {
    console.log(error);
    next(error);
  }
};

// Custom Methods

export const getOwnerPets = async (req, res, next) => {
  try {
    // Params
    const {ownerId} = req.body;
    
    // Find owner's dogs
    const dogs = await Dog.findAll({
      where: {
        ownerId: ownerId
      }
    });
    res.json(dogs);
  } catch (error) {
    console.log(error);
    next(error);
  }
};
