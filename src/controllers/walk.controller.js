import Walk from "../models/Walk";
import Walker from "../models/Walker";
import { Op } from "sequelize";

export const createWalk = async (req, res, next) => {
  try {
    // Params
    const {
      price,
      distanceTraveled,
      stepsWalked,
      time,
      status,
      dogId,
      ownerId,
      walkerId,
      neighborhoodGroupId,
    } = req.body;

    const date = new Date();

    const assignedWalker = await Walker.findOne({
      where: { id: walkerId },
    });

    if (!assignedWalker.status) {
      return res.status(401).json();
    }

    // Create Walk
    const newWalk = await Walk.create({
      date,
      price,
      distanceTraveled,
      stepsWalked,
      time,
      status,
      dogId,
      ownerId,
      walkerId,
      neighborhoodGroupId,
    });

    await assignedWalker.update({
      status: false,
    });

    res.json(newWalk);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const getAllWalks = async (req, res, next) => {
  try {
    // Get all elements in database
    const walks = await Walk.findAll();

    res.json(walks);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const getWalkById = async (req, res, next) => {
  try {
    // Params
    const { walkId } = req.params;

    // Find Walk in database
    const walk = await Walk.findOne({
      where: { id: walkId },
    });

    res.json(walk);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const updateWalkById = async (req, res, next) => {
  try {
    // Params
    const { walkId } = req.params;
    const {
      price,
      distanceTraveled,
      stepsWalked,
      time,
      status,
      dogId,
      ownerId,
      walkerId,
      neighborhoodGroupId,
    } = req.body;

    // Find Walk in database
    const walk = await Walk.findOne({
      where: { id: walkId },
    });

    // Update Walk Setting
    await walk.update({
      price,
      distanceTraveled,
      stepsWalked,
      time,
      status,
      dogId,
      ownerId,
      walkerId,
      neighborhoodGroupId,
    });

    res.json(walk);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const deleteWalkById = async (req, res, next) => {
  try {
    // Params
    const { walkId } = req.params;

    // Delete Walk in database
    await Walk.destroy({
      where: { id: walkId },
    });

    res.json();
  } catch (error) {
    console.log(error);
    next(error);
  }
};

// Custom Methods
export const getOwnerWalks = async (req, res, next) => {
  try {
    // Params
    const { ownerId } = req.body;

    // Find Walks in database
    const walks = await Walk.findAll({
      where: { ownerId },
      order: [["date", "DESC"]],
    });

    res.json(walks);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const getWalkerWalks = async (req, res, next) => {
  try {
    // Params
    const { walkerId } = req.body;

    // Find Walks in database
    const walks = await Walk.findAll({
      where: { walkerId },
      order: [["date", "DESC"]],
    });

    res.json(walks);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const getStatusOwnerWalks = async (req, res, next) => {
  try {
    // Params
    const { ownerId } = req.body;
    const { status } = req.params;

    // Find Walks in database
    const walks = await Walk.findAll({
      where: { [Op.and]: [{ ownerId }, { status }] },

      order: [["date", "DESC"]],

      limit: 10,
    });

    res.json(walks);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const getStatusWalkerWalks = async (req, res, next) => {
  try {
    // Params
    const { walkerId } = req.body;
    const { status } = req.params;

    // Find Walks in database
    const walks = await Walk.findAll({
      where: { [Op.and]: [{ walkerId }, { status }] },

      order: [["date", "DESC"]],

      limit: 10,
    });

    res.json(walks);
  } catch (error) {
    console.log(error);
    next(error);
  }
};
