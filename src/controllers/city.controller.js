import City from "../models/City";

export const createCity = async (req, res, next) => {
  try {
    // Params
    const { name } = req.body;

    // Create City
    const newCity = await City.create({
      name,
    });

    res.json(newCity);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const getAllCities = async (req, res, next) => {
  try {
    // Get all elements in database
    const cities = await City.findAll();

    res.json(cities);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const getCityById = async (req, res, next) => {
  try {
    // Params
    const { cityId } = req.params;

    // Find City in database
    const city = await City.findOne({
      where: { id: cityId },
    });

    res.json(city);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const updateCityById = async (req, res, next) => {
  try {
    // Params
    const { cityId } = req.params;
    const { name } = req.body;

    // Find City in database
    const city = await City.findOne({
      where: { id: cityId },
    });

    // Update City Setting
    await city.update({ name });

    res.json(city);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export const deleteCityById = async (req, res, next) => {
  try {
    // Params
    const { cityId } = req.params;

    // Delete City in database
    await City.destroy({
      where: { id: cityId },
    });

    res.json();
  } catch (error) {
    console.log(error);
    next(error);
  }
};
