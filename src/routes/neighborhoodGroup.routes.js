import { Router } from "express";

import * as neighborhoodGroupController from "../controllers/neighborhoodGroup.controller";

const router = Router();

// Routes

// POST /api/neighborhoodGroups/
router.post("/", neighborhoodGroupController.createNeighborhoodGroup);

// GET /api/neighborhoodGroups/
router.get("/", neighborhoodGroupController.getAllNeighborhoodGroups);

// GET /api/neighborhoodGroups/:neighborhoodGroupId
router.get(
  "/:neighborhoodGroupId",
  neighborhoodGroupController.getNeighborhoodGroupById
);

// PUT /api/neighborhoodGroups/:neighborhoodGroupId
router.put(
  "/:neighborhoodGroupId",
  neighborhoodGroupController.updateNeighborhoodGroupById
);

// DELETE /api/neighborhoodGroups/:neighborhoodGroupId
router.delete(
  "/:neighborhoodGroupId",
  neighborhoodGroupController.deleteNeighborhoodGroupById
);

export default router;
