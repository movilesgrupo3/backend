import { Router } from "express";

import * as breedController from "../controllers/breed.controller";

const router = Router();

// Routes

// POST /api/breeds/
router.post("/", breedController.createBreed);

// GET /api/breeds/
router.get("/", breedController.getAllBreeds);

// GET /api/breeds/:breedId
router.get("/:breedId", breedController.getBreedById);

// PUT /api/breeds/:breedId
router.put("/:breedId", breedController.updateBreedById);

// DELETE /api/breeds/:breedId
router.delete("/:breedId", breedController.deleteBreedById);

export default router;
