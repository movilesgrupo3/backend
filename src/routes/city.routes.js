import { Router } from "express";

import * as cityController from "../controllers/city.controller";

const router = Router();

// Routes

// POST /api/cities/
router.post("/", cityController.createCity);

// GET /api/cities/
router.get("/", cityController.getAllCities);

// GET /api/cities/:cityId
router.get("/:cityId", cityController.getCityById);

// PUT /api/cities/:cityId
router.put("/:cityId", cityController.updateCityById);

// DELETE /api/cities/:cityId
router.delete("/:cityId", cityController.deleteCityById);

export default router;
