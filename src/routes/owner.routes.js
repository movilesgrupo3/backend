import { Router } from "express";

import * as ownerController from "../controllers/owner.controller";
import * as authMiddleware from "../middlewares/authMiddleware";

const router = Router();

// Routes

// POST /api/owners/
router.post("/", ownerController.createOwner);

// GET /api/owners/
router.get("/", ownerController.getAllOwners);

// GET /api/owners/ownerFavoriteWalkers/
router.get("/ownerFavoriteWalkers", authMiddleware.verifyToken, ownerController.getOwnerFavoriteWalkers)

// Custom Routes

// GET /api/owners/ownerPets
router.get(
    "/ownerPets",
    authMiddleware.verifyToken,
    ownerController.getOwnerPets
);

// GET /api/owners/:ownerId
router.get("/:ownerId", ownerController.getOwnerById);

// GET /api/owners/:ownerUId
router.get("/uid/:ownerUId", ownerController.getOwnerByUId);

// PUT /api/owners/:ownerId
router.put("/:ownerId", ownerController.updateOwnerById);

// DELETE /api/owners/:ownerId
router.delete("/:ownerId", ownerController.deleteOwnerById);

export default router;
