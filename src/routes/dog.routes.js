import { Router } from "express";

import * as dogController from "../controllers/dog.controller";
import * as authMiddleware from "../middlewares/authMiddleware";

const router = Router();

// Routes

// POST /api/dogs/createw_token
router.post("/createw_token", authMiddleware.verifyToken, dogController.createDog)

// POST /api/dogs/
router.post("/", dogController.createDog);

// GET /api/dogs/
router.get("/", dogController.getAllDogs);

// GET /api/dogs/:dogId
router.get("/:dogId", dogController.getDogById);

// PUT /api/dogs/:dogId
router.put("/:dogId", dogController.updateDogById);

// DELETE /api/dogs/:dogId
router.delete("/:dogId", dogController.deleteDogById);

export default router;
