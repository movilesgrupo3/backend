import { Router } from "express";

import * as walkerController from "../controllers/walker.controller";

const router = Router();

// Routes

// POST /api/walkers/
router.post("/", walkerController.createWalker);

// GET /api/walkers/
router.get("/", walkerController.getAllWalkers);

// GET /api/walkers/:ownerPosition
router.get("/nearwalkers/:ownerPosition", walkerController.getAllWalkersNear);

// GET /api/walkers/:walkerId
router.get("/:walkerId", walkerController.getWalkerById);

// GET /api/owners/:ownerUId
router.get("/uid/:walkerUId", walkerController.getWalkerByUId);

// PUT /api/walkers/:walkerId
router.put("/:walkerId", walkerController.updateWalkerById);

// DELETE /api/walkers/:walkerId
router.delete("/:walkerId", walkerController.deleteWalkerById);



export default router;
