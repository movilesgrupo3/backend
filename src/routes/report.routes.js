import { Router } from "express";

import * as reportController from "../controllers/report.controller";

const router = Router();

// Routes

// POST /api/reports/
router.post("/", reportController.createReport);

// GET /api/reports/
router.get("/", reportController.getAllReports);

// GET /api/reports/:reportId
router.get("/:reportId", reportController.getReportById);

// PUT /api/reports/:reportId
router.put("/:reportId", reportController.updateReportById);

// DELETE /api/reports/:reportId
router.delete("/:reportId", reportController.deleteReportById);

export default router;
