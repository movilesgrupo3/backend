import { Router } from "express";

import * as walkController from "../controllers/walk.controller";
import * as authMiddleware from "../middlewares/authMiddleware";

const router = Router();

// Routes

// POST /api/walks/
router.post("/", walkController.createWalk);

// GET /api/walks/
router.get("/", walkController.getAllWalks);

// GET /api/walks/ownerWalks -- CUSTOM
router.get(
  "/ownerWalks",
  authMiddleware.verifyToken,
  walkController.getOwnerWalks
);

router.get(
    "/walkerWalks",
    authMiddleware.verifyToken,
    walkController.getWalkerWalks
);

// GET /api/walks/statusOwnerWalks -- CUSTOM
router.get(
  "/statusOwnerWalks/:status",
  authMiddleware.verifyToken,
  walkController.getStatusOwnerWalks
);

router.get(
    "/statusWalkerWalks/:status",
    authMiddleware.verifyToken,
    walkController.getStatusWalkerWalks
);

// GET /api/walks/:walkId
router.get("/:walkId", walkController.getWalkById);

// PUT /api/walks/:walkId
router.put("/:walkId", walkController.updateWalkById);

// DELETE /api/walks/:walkId
router.delete("/:walkId", walkController.deleteWalkById);

export default router;
