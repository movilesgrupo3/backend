import * as admin from "firebase-admin";
//import serviceAccount from "../common/canem-app-firebase-adminsdk.json";
//import * as serviceAccount from "../common/canem-app-firebase-adminsdk.json";

import Owner from "../models/Owner";
import Walker from "../models/Walker";

// Firebase Authentication Settings
let firebaseDefaultApp = admin.initializeApp({
  credential: admin.credential.cert({
    type: "service_account",
    project_id: "canem-app",
    private_key_id: "2616b17284736972c297f9b78345be0e43769ee4",
    private_key:
      "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCKU0fGLCmR6cS0\nTyYc5Dgo9jDOBugXOa6Xaeh+NSWANTEwpygDFaehfvTiJWHdc68hOVWn78JU2HyK\nIqRs3BEjV9/a4DiqSn4RRZI5lnR99LkBzrXrmCGctsF/+2XH36jqnmwqY8JMtXOn\nTOey1PtuaRMW2SrdzY7EbSmPOElBnkJxkpByEkAJmxhT75kdt3taMEfFSnPVHQZ6\nNRqYWbDc5V1N7gJpQy4yQu2/7zUBnSr+1hNJkXtL5ovUN7UmKrDJsv+qCeRO/wiW\nLILegcppdGaHLNEL/6kwzpYyugO4TMoJYa5n2UyLqsBLnnITi9R2DFCGYyHS9iCY\ndB5rs7wLAgMBAAECggEAFjcw4FGIbaBTULgjgWFf6ag8h2JRlSVU9olyZw6nErO4\nTj2Sbghkz0miD838LUJenqHkMWAO4GySd+b/bGJduETtNWxsfWEQZTq8tNYGqJIM\nwo8N5aQdxdsOMb1RB+xSZzgBXD+AXayIwhZWYgDVztI+WB2RxmS0qLR1h8R883Y4\neI/4vfhixtWq5NuviBQbsDqeLpo+Mzn66F5R+xIEIc9okLjkkBAzw7o6FOO9/t/D\nAGrjHPVSyvBfDA3XbkKgyyZcX18kL/5jjBMLEe+bH/kjB8AGXYTodipCMheuHvVL\naFRYYEeDF+GSyCDACsm3xBXY4xFaCvhO/qC3h/46VQKBgQC8iRmkwHy8B6IOaYUB\n5hISnX3wWUib4rNInjRUbthbE4CYwpklvEO7QuQ6k+EU69tQ+ubNrW9GukJxyKqd\nnKUdnORgtfY7tkd/Vag9iyj7Kc9Ga/+Azzdcw7ZwENHHqIO/Vcfm6Vq6H5caOrxl\ndHIKKdB5Dc4fvbrQrgz87Q/WJQKBgQC70qUU8eyqyJlNXy8Nq7HRp0xQN56jrhBt\nrH8w1BMdlUI4tu3y/f7VO9BWHR0ZJoI9oinTe7OxWJDTtUKCXlx1Gz4yb4jKlUYG\nYHkzabsd/SFl424OM9NHxEhuaJbu+3RLo1K+AWv+PmJQWcl+Aloey/tQ/1Tntc7y\nZkf+tZO6bwKBgAp/U7cxpIBPqH1G8MnvRnhU3otAXWOYlsTnUiCVX2SKzHE0/JP2\nTIsjqVN9ol2o2DhFEA7JplV4z3RIaP+HlkX0015zIzel6Ys0eL6cffamGE1ifAJv\nBzUaDJFmzldgkZ1YqF26BnwwgBhMxuqbNjJMq7fhxeVMihdOaZi3llclAoGAUwrf\nUlNkgF4Q6dSbyT+Qb1G4u/7bdSMQwGz0D3K0wO+tR8YZjsYeVckMZAabRvNvwjBg\nrdCTsmWmIvSRY3K1DlHtPf9yS5+URl2BOOoWgTvZ+GKF1mGzmJeALtCvaxpiYM7l\nVnzqco7CiSQpoj0H+3XUuAssTwZjaWkvi75cgqECgYEAhFnHR2qrRTiaZH/OY9OK\nboRznVn0jSF9dlEIxg8jSvkX/9pRulhvqQmnb54ZSsUZcOFWBmpWPA2b5juR+9wm\nU7gsCH2mp/7s1IbI+cSaFpNqnkbWsW6nZ2uql4PZmH3HuQkjKxHneGrQgfq9YfSS\nF5azB5MfeCqxsIxHU60svjk=\n-----END PRIVATE KEY-----\n",
    client_email: "firebase-adminsdk-706ep@canem-app.iam.gserviceaccount.com",
    client_id: "111297635511949144958",
    auth_uri: "https://accounts.google.com/o/oauth2/auth",
    token_uri: "https://oauth2.googleapis.com/token",
    auth_provider_x509_cert_url: "https://www.googleapis.com/oauth2/v1/certs",
    client_x509_cert_url:
      "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-706ep%40canem-app.iam.gserviceaccount.com",
  }),
});
let defaultAuth = firebaseDefaultApp.auth();

export const verifyToken = async (req, res, next) => {
  try {
    // Found token
    let idToken = null;
    if (
      req.headers.authorization &&
      req.headers.authorization.split(" ")[0] === "Bearer"
    ) {
      idToken = req.headers.authorization.split(" ")[1];
    }

    if (!idToken) {
      return res.status(403).json({ message: "No token provided" });
    }

    // Validate token
    const decodedToken = await defaultAuth.verifyIdToken(idToken);
    const uid = decodedToken.uid;
    console.log("uid", uid);

    // Found user
    const owner = await Owner.findOne({
      where: {
        uid,
      },
    });

    const walker = await Walker.findOne({
      where: {
        uid,
      },
    });

    if (owner) {
      req.body.ownerId = owner.id;
      console.log(owner.id);
    } else if (walker) {
      req.body.walkerId = walker.id;
      console.log(walker.id);
    } else {
      return res.status(404).json({ message: "No user found" });
    }

    // Token is valid
    next();
  } catch (error) {
    console.log(error);
    return res.status(401).json({ message: "Unauthorized" });
  }
};
